# Styles

If you want to edit CSS styles directly, make sure you delete the `scss/styles.scss`
file and change the `prepublish` script in `package.json` to

```bash
yarn run dist
```

Otherwise your CSS file will be overwritten while compiling!
