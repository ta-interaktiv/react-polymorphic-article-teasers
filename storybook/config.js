import {configure} from '@kadira/storybook';

function loadStories() {
  'use strict';

  require('../stories/index.js');
}

configure(loadStories, module);
