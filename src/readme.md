# Source
Add your component's source code in this index.js; as a ES6 module.

- Show possible problems in your code using
  ```bash
  yarn run lint
  ```

- Fix those problems, if possible:
  
  ```bash
  yarn run fix
  ```

- Transpile your code to ES5 into the `dist` directory:

  ```bash
  yarn run dist
  ```
  
- Generate documentation / readme file from documentation in code (using JSDoc):
  
  ```bash
  yarn run generate-docs
  ```
  
## Test using Storybook

Add stories to `stories/index.js`. Then run

```bash
yarn run storybook
```

and open your browser on <http://localhost:9001/>.

## Publish the whole component to NPM

1. Check if the information in `package.json` is correct. If not, remove "name",
"version", "author", "repository" and "description" and run
   ```bash
   npm init
   ```
  
2. Update version number, if necessary, using one of
   ```bash
   npm version patch
   npm version minor
   npm version major
   ```
   
3. Publish to NPM using
   ```bash
   npm publish
   ```
   This will run the "prepublish" task which will compile the SCSS and transpile
   the ES6 module to a ES5 version in `dist` (see above),
   before pushing the package to NPM.
