/**
 * @module PolymorphicArticleTeasers
 * @example <caption>Format of the CSV</caption>
 * ID
 * 29014315
 * 14861953
 * 29390761
 * 16418561
 * 30299320
 * 10332177
 * 20874355
 * 30846102
 * 22946013
 */
import React from 'react'
import { csv } from 'd3-request'
import ArticleTeasers from '@ta-interaktiv/react-article-teasers'

const requestState = {
  LOADING: 'loading',
  ERROR: 'error',
  READY: 'ready'
}

/**
 * Load article teasers using an external list.
 * @version 1.0
 * @class
 * @extends React.Component
 * @example
 * import ArticleTeasers from '@ta-interaktiv/react-polymorphic-article-teasers'
 * import {sortMode} from '@ta-interaktiv/react-article-teasers'
 *
 * function SomeComponent (props) {
 *   return (
 *     <ArticleTeasers
 *       srcUrl='//interaktiv.tagesanzeiger.ch/2017/200-jahre-velo/data/articles.csv'
 *       sorting={sortMode.REVERSE_CHRONOLOGICALLY_BY_UPDATE_DATE} />
 *   )
 * }
 */
export default class extends React.Component {
  /**
   * Create a new externally loaded article teaser list.
   *
   * @param {string} props.srcUrl The URL where the CSV is to be found.
   * @param {string} [props.numberOfCards=two] How many article teaser
   * should be shown in a row on desktop devices. As a number word. ('one', 'two',
   * 'three', etc.)
   * @param {sortMode} [props.sorting=manual] The sorting mode. Choose from
   * one of the options from {@link sortMode} of the original Article Teasers component.
   * @param {string} [props.additionalClasses=] Additional classes to be
   * passed to the cards element. As defined in the
   * {@link http://semantic-docs.xeophin.com/views/card.html|Card
   *   documentation}.
   */
  constructor (props) {
    super(props)

    this.state = {
      articleIds: [],
      requestState: requestState.LOADING
    }
  }

  componentDidMount () {
    csv(this.props.srcUrl, (error, data) => {
      if (error) {
        console.group('Article ID List Loader')
        console.log('The desired data could not be loaded.', error)
        console.groupEnd()
        this.setState({
          requestState: requestState.ERROR
        })
        return
      }
      let articleIds = []

      data.map(articleObject => articleIds.push(articleObject.ID))
      console.log(articleIds)

      this.setState({
        articleIds: articleIds,
        requestState: requestState.READY
      })
    })
  }

  render () {
    if (this.state.requestState !== requestState.READY) {
      return false
    }

    const {articleIds, ...passThroughProps} = this.props
    return <ArticleTeasers {...passThroughProps} articleIds={this.state.articleIds} />
  }
}
