import React from 'react'
import {storiesOf, action} from '@kadira/storybook'
import backgrounds from 'react-storybook-addon-backgrounds'
import ArticleTeasers from '../src/index'
import {sortMode} from '@ta-interaktiv/react-article-teasers'
import 'ta-semantic-ui/semantic/dist/components/reset.css'
import 'ta-semantic-ui/semantic/dist/components/site.css'
import 'ta-semantic-ui/semantic/dist/components/segment.css'

storiesOf('Article Teaser', module)
  .addDecorator(backgrounds([
    {name: 'white', value: '#FFFFFF', default: true},
    {name: 'dark', value: '#060606'}
  ]))
  .add('Default', () => (
    <ArticleTeasers srcUrl='//interaktiv.tagesanzeiger.ch/2017/200-jahre-velo/data/articles.csv' />
  ))
  .add('Failing', () => (
    <ArticleTeasers srcUrl='//interaktiv.tagesanzeiger.ch/2017/200-jahre-velo/dada/articles.csv' />
  ))
  .add('Grey Inverted', () => (
    <ArticleTeasers srcUrl='//interaktiv.tagesanzeiger.ch/2017/200-jahre-velo/data/articles.csv' additionalClasses='grey inverted doubling' numberOfCards='five' />
  ))

  .add('Sorted', () => (
    <ArticleTeasers srcUrl='//interaktiv.tagesanzeiger.ch/2017/200-jahre-velo/data/articles.csv'
      numberOfCards='five' sorting={sortMode.REVERSE_CHRONOLOGICALLY_BY_UPDATE_DATE} />
  ))
