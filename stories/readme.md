# Stories

In order to test your components, add them as "Stories" in `index.js` in this directory.

Test using

```bash
yarn run storybook
```
