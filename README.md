# Article Teasers using external sources (Polymorphic Article Teasers)
Shows an article teaser list like @ta-interaktiv/react-article-teasers, but allows you to define an external source with a list of article IDs for quick updating.

## Installation

```bash
yarn add @ta-interaktiv/react-polymorphic-article-teasers
```

<a name="module_PolymorphicArticleTeasers"></a>

## PolymorphicArticleTeasers
**Example** *(Format of the CSV)*  
```js
ID
29014315
14861953
29390761
16418561
30299320
10332177
20874355
30846102
22946013
```

* [PolymorphicArticleTeasers](#module_PolymorphicArticleTeasers)
    * [module.exports](#exp_module_PolymorphicArticleTeasers--module.exports) ⇐ <code>React.Component</code> ⏏
        * [new module.exports()](#new_module_PolymorphicArticleTeasers--module.exports_new)

<a name="exp_module_PolymorphicArticleTeasers--module.exports"></a>

### module.exports ⇐ <code>React.Component</code> ⏏
Load article teasers using an external list.

**Kind**: Exported class  
**Extends**: <code>React.Component</code>  
**Version**: 1.0  
<a name="new_module_PolymorphicArticleTeasers--module.exports_new"></a>

#### new module.exports()
Create a new externally loaded article teaser list.


| Param | Type | Default | Description |
| --- | --- | --- | --- |
| props.srcUrl | <code>string</code> |  | The URL where the CSV is to be found. |
| [props.numberOfCards] | <code>string</code> | <code>&quot;two&quot;</code> | How many article teaser should be shown in a row on desktop devices. As a number word. ('one', 'two', 'three', etc.) |
| [props.sorting] | <code>sortMode</code> | <code>manual</code> | The sorting mode. Choose from one of the options from [sortMode](sortMode) of the original Article Teasers component. |
| [props.additionalClasses=] | <code>string</code> |  | Additional classes to be passed to the cards element. As defined in the {@link http://semantic-docs.xeophin.com/views/card.html|Card   documentation}. |

**Example**  
```js
import ArticleTeasers from '@ta-interaktiv/react-polymorphic-article-teasers'
import {sortMode} from '@ta-interaktiv/react-article-teasers'

function SomeComponent (props) {
  return (
    <ArticleTeasers
      srcUrl='//interaktiv.tagesanzeiger.ch/2017/200-jahre-velo/data/articles.csv'
      sorting={sortMode.REVERSE_CHRONOLOGICALLY_BY_UPDATE_DATE} />
  )
}
```
